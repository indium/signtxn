# docker run --rm -p 5000:5000 -d $(docker build -q .)

FROM python:3.6-jessie

RUN pip3 install Flask==1.0.2
RUN pip3 install web3==4.5.0

COPY . /app
WORKDIR /app

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3"]
CMD ["signtxn.py"]