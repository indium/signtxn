from flask import Flask
from flask import request
from flask import render_template
from web3 import Web3
from web3.middleware import geth_poa_middleware
import json
from hexbytes import HexBytes
import time

class HexJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, HexBytes):
            return obj.hex()
        return super().default(obj)

app = Flask(__name__)

@app.route('/', methods=['GET'])
def transfer_get():
    return render_template('form.html')

@app.route('/', methods=['POST'])
def transfer_post():
    rpc_node_url = request.form.get('rpc')
    receiver = request.form.get('receiver')
    sender = request.form.get('sender')
    privkey = request.form.get('privkey')
    amount = int(request.form.get('amount'))
    gaslimit = int(request.form.get('gaslimit'))
    gasprice = int(request.form.get('gasprice'))
    chain = int(request.form.get('chain'))
    data = request.form.get('data')

    w3 = Web3(Web3.HTTPProvider(rpc_node_url))
    w3.middleware_stack.inject(geth_poa_middleware, layer=0)
    w3.eth.getBalance(receiver)
    w3.eth.getBalance(sender)

    transaction = {
            'to': receiver,
            'value': amount,
            'gas': gaslimit,
            'gasPrice': gasprice,
            'nonce': w3.eth.getTransactionCount(sender),
            'chainId': chain,
            'data': str.encode(data)
    }

    signed = w3.eth.account.signTransaction(transaction, privkey)
    txn_hash = w3.eth.sendRawTransaction(signed.rawTransaction)
    # time.sleep(5)
    # receipt = w3.eth.getTransactionReceipt(txn_hash)
    # return json.dumps(dict(receipt), cls=HexJsonEncoder)
    return json.dumps(txn_hash, cls=HexJsonEncoder)


if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')